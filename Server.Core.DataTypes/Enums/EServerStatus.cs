﻿namespace Server.Core.DataTypes.Enums
{
    /// <summary>
    /// Статусы приложения
    /// </summary>
    public enum EServerStatus
    {
        /// <summary>
        /// Простой
        /// </summary>
        Idle = 0,
        
        /// <summary>
        /// Подготовка к запуску
        /// </summary>
        Starting = 1,

        /// <summary>
        /// Выполнение задач
        /// </summary>
        Running = 2,

        /// <summary>
        /// Завершение задач
        /// </summary>
        FinalizationTasks = 3,

        /// <summary>
        /// Остановлен
        /// </summary>
        Stopped = 4
    }
}