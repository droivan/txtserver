﻿using System.Threading.Tasks;
using Server.Core.DataTypes.Enums;
using Server.DtoModel;

namespace Server.Core.Interfaces.DomainLogic
{
    /// <summary>
    /// Интерфейс сервиса управления сервером
    /// </summary>
    public interface IFileCalсulationDomainService
    {
        /// <summary>
        /// Получить состояние работы сервиса
        /// </summary>
        /// <returns>Состояние сервиса</returns>
        Task<EServerStatus> GetStatusAsync();

        /// <summary>
        /// Запуск сервиса анализа текстовых файлов
        /// </summary>
        /// <param name="config">Настройка каталогов приложения</param>
        /// <returns>Состояние сервиса</returns>
        Task<EServerStatus> StartAsync(DirectoryConfigurationDto config);

        /// <summary>
        /// Остановка сервиса
        /// </summary>
        /// <returns>Состояние сервиса</returns>
        Task<EServerStatus> StopAsync();
    }
}