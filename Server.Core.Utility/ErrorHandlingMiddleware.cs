﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Serilog;
#if RELEASE
using Newtonsoft.Json;
#endif
namespace Server.Core.Utility
{
    /// <summary>
    /// Middleware логирования ошибки 
    /// </summary>
    public class ErrorHandlingMiddleware
    {
        /// <summary>
        /// Делегат вызовов middleware
        /// </summary>
        private readonly RequestDelegate next;

        /// <summary>
        /// Конструктор
        /// </summary>
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        // Вызов делегата
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        /// <summary>
        /// Обработчик исключений
        /// </summary>
        /// <param name="context">Контекст</param>
        /// <param name="ex">Исключение</param>
        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            var code = HttpStatusCode.BadRequest;
            var resultJson = JsonConvert.SerializeObject(ex);
            Log.Error(resultJson, ex);

#if RELEASE
            // В RELEASE отдавать статус код без подробной ошибки
            string message = "Error";
#endif
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

#if RELEASE
            return context.Response.WriteAsync(JsonConvert.SerializeObject(message));
#else
            return context.Response.WriteAsync(resultJson);
#endif
        }
    }
}