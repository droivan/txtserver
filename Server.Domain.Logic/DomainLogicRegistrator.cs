﻿using Microsoft.Extensions.DependencyInjection;
using Server.Core.Interfaces.DomainLogic;
using Server.Domain.Logic.Services;

namespace Server.Domain.Logic
{
    /// <summary>
    /// Класс - обертка для сокрытия подключения зависимостей уровня доменной логики
    /// </summary>
    public static class DomainLogicRegistrator
    {
        /// <summary>
        /// Подключение зависимостей уровня доменной логики
        /// </summary>
        /// <param name="services">Коллекция служб приложения</param>
        public static IServiceCollection UseDomainLogic(this IServiceCollection services)
        {
            services.AddSingleton<IFileCalсulationDomainService, FileCalсulationDomainService>();
            return services;
        }
    }
}