﻿using AutoMapper;

namespace Server.Domain.Logic.Mapping
{
    /// <summary>
    /// Конфигуратор автомаппера
    /// </summary>
    public class DtoMappingProfile: Profile
    {
        /// <summary>
        /// Конструктор профиля автомапера
        /// </summary>
        public DtoMappingProfile()
        {

        }
    }
}