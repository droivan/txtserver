﻿using System.IO;
using System.Threading.Tasks;
using Server.Core.DataTypes.Enums;
using Server.Core.Interfaces.DomainLogic;
using Server.DtoModel;

namespace Server.Domain.Logic.Services
{
    /// <summary>
    /// Сервис, управляющий работой с файлами 
    /// </summary>
    public sealed class FileCalсulationDomainService: IFileCalсulationDomainService
    {
        /// <summary>
        /// Путь к папке с исходными документами в формате TXT 
        /// </summary>
        private string _sourcePath;

        /// <summary>
        /// Путь к папке с результатами
        /// </summary>
        private string _resultPath;

        /// <summary>
        /// Состояние сервиса
        /// </summary>
        private EServerStatus _status;

        /// <summary>
        /// Конструтор
        /// </summary>
        public FileCalсulationDomainService()
        {
            _status = EServerStatus.Idle;
        }

        /// <inheritdoc />
        public async Task<EServerStatus> GetStatusAsync() =>
            await Task.FromResult(_status);

        /// <inheritdoc />
        public async Task<EServerStatus> StartAsync(DirectoryConfigurationDto config)
        {
            if (!Directory.Exists(config.SourceDirectory))
            {
                throw new DirectoryNotFoundException("Папка с исходными документами не найдена");
            }
            if (!Directory.Exists(config.ResultDirectory))
            {
                throw new DirectoryNotFoundException("Папка с результатами не найдена");
            }

            if (_status == EServerStatus.Starting)
            {
                // переход в Running
                _status = EServerStatus.Running;
            }
            if(_status == EServerStatus.Idle)
            {
                // переход в starting
                _status = EServerStatus.Starting;

                _sourcePath = config.SourceDirectory;
                _resultPath = config.ResultDirectory;
            }
             

            return _status;
        }

        /// <inheritdoc />
        public async Task<EServerStatus> StopAsync()
        {
            throw new System.NotImplementedException();
        }
    }
}