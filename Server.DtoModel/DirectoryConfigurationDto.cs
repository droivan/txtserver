﻿using System.Runtime.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace Server.DtoModel
{
    /// <summary>
    /// Модель настройки директорий приложения
    /// </summary>
    public class DirectoryConfigurationDto
    {
        /// <summary>
        /// Путь к папке с исходными документами в формате TXT
        /// </summary>
        [Required]
        public string SourceDirectory { get; set; }

        /// <summary>
        /// Путь к папке с результатами
        /// </summary>
        [Required]
        public string ResultDirectory { get; set; }
        
    }
}