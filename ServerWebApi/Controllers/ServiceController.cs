﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Server.Core.DataTypes.Enums;
using Server.Core.Interfaces.DomainLogic;
using Server.DtoModel;

namespace Server.WebApi.Controllers
{
    [Produces("application/json")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private readonly IFileCalсulationDomainService _fileCalсulationDomainService;
        public ServiceController(IFileCalсulationDomainService fileCalсulationDomainService)
        {
            _fileCalсulationDomainService = fileCalсulationDomainService;
        }
        
        /// <summary>
        /// Получение состояния сервиса
        /// </summary>
        /// <returns>Состояние сервиса</returns>
        [HttpGet, Route("api/public/status")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(EServerStatus))]
        public async Task<IActionResult> GetStatusAsync()
        {
            return Ok(await _fileCalсulationDomainService.GetStatusAsync());
        }

        /// <summary>
        /// Запуск сервиса анализа текстовых файлов
        /// </summary>
        /// <param name="config">Настройка каталогов приложения</param>
        /// <returns>Состояние сервиса</returns>
        [HttpPost, Route("api/public/start")]
        public async Task<IActionResult> StartAsync([FromBody] DirectoryConfigurationDto config)
        {
            return Ok(await _fileCalсulationDomainService.StartAsync(config));
        }

        /// <summary>
        /// Остановка сервиса
        /// </summary>
        /// <returns>Состояние сервиса</returns>
        [HttpPost, Route("api/public/stop")]
        public async Task<IActionResult> StopAsync()
        {
            return Ok();
        }

    }
}
