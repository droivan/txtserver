﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace threadTests
{
    class Program
    {
        private static int x = 0;
        
        static void Main(string[] args)
        {
            Task task1 = new Task(() => {
                Console.WriteLine($"Id задачи: {Task.CurrentId}");
            });

            Task task2 = task1.ContinueWith(Display);

            Task task3 = task1.ContinueWith((Task t) =>
            {
                Console.WriteLine($"Id задачи: {Task.CurrentId}");
            });

            Task task4 = task1.ContinueWith((Task t) =>
            {
                Console.WriteLine($"Id задачи: {Task.CurrentId}");
            });

            task1.Start();
            Console.ReadLine();
        }

        static int Sum(int a, int b) => a + b;

        static void Display(Task t)
        {
            Console.WriteLine($"Id задачи: {Task.CurrentId}");
        }

        public class Reader
        {
            static Semaphore sem = new Semaphore(1, 1);
            private Thread myThread;
            private int count = 3;

            public Reader(int i)
            {
                myThread = new Thread(Read);
                myThread.Name = $"Читатель {i.ToString()}";
                myThread.Start();
            }

            public void Read()
            {
                while (count > 0)
                {
                    sem.WaitOne();
                    Console.WriteLine($"{Thread.CurrentThread.Name} входит в библиотеку");
                    Console.WriteLine($"{Thread.CurrentThread.Name} читает");
                    Thread.Sleep(1000);
                    Console.WriteLine($"{Thread.CurrentThread.Name} покидает в библиотеку");
                    sem.Release();
                    count--;
                    Thread.Sleep(1000);
                }
            }
        }

        public static void Count(object obj)
        {
            x = (int) obj;
            for (int i = 1; i < 9; i++, x++)
            {
                Console.WriteLine($"{x * i}");
            }
        }

        public class Counter
        {
            public int x;
            public int y;

            public Counter(int _x, int _y)
            {
                x = _x;
                y = _y;
            }
            public void Count()
            {
                for (int i = 1; i < 9; i++)
                {
                    Console.WriteLine("2 поток");
                    Console.WriteLine(i * x * y);
                    Thread.Sleep(400);
                }
            }
        }
    }
}
